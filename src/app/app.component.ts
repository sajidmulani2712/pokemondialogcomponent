import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  pokemonList = ["Charizard", "Mewtwo", "Pikachu"];
  chosenPokemon = '';
  showModal = false;

   choosePokemon() {
    const randomIndex = Math.floor(Math.random() * this.pokemonList.length);
    this.chosenPokemon = this.pokemonList[randomIndex];
  }

  toggleModals() {
    this.showModal = !this.showModal;
    console.log("sajid");
  }

  ngOnInit() {
    this.choosePokemon();
  }
}
