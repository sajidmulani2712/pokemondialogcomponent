import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  pokemonList = ["Charizard", "Mewtwo", "Pikachu"];
  chosenPokemon = '';
  showModal = false;

// @Output() sendModal= new EventEmitter();

@Input() name:any;

  choosePokemon() {
    const randomIndex = Math.floor(Math.random() * this.pokemonList.length);
    this.chosenPokemon = this.pokemonList[randomIndex];
  }

  toggleModal() {
    this.showModal = !this.showModal;
    // this.sendModal.emit(this.showModal);
  }

  constructor() { }

  ngOnInit(): void {
    this.choosePokemon();
  }

}
