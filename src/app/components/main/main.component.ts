import { Component, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  pokemonList = ["Charizard", "Mewtwo", "Pikachu"];
  chosenPokemon = '';
  showModal = false;

  @Output() sendModal= new EventEmitter();

  choosePokemon() {
    const randomIndex = Math.floor(Math.random() * this.pokemonList.length);
    this.chosenPokemon = this.pokemonList[randomIndex];
  }

  toggleModal() {
    this.showModal = !this.showModal;
    this.sendModal.emit(this.showModal);
  }

  constructor() { }

  ngOnInit(): void {
    this.choosePokemon();
  }

}
